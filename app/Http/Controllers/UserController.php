<?php

namespace App\Http\Controllers;

use App\Models\User;
use Inertia\Inertia;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function destroy(User $user)
    {
        if ($user->id===Auth::user()->id) {
            if ($user->company!=null) {
                $user->company()->delete();
            }
            $user->delete();

            return Redirect::route('welcome');
        }elseif (Auth::user()->is_admin===1) {

            if ($user->company!=null) {
                $user->company()->delete();
            }
            $user->delete();
            return Redirect::route('companies.index');
        }
        return Redirect::route('welcome');
        
    }
}
// ||Auth::user()->roles->first()->name==='admin'