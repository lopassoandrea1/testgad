<?php

namespace App\Http\Controllers;

use App\Models\User;
use Inertia\Inertia;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Imports\CompanyImport;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CompanyRequest;
use Illuminate\Support\Facades\Redirect;

class CompanyController extends Controller
{
    public function index()
    {
        $users= User::all();
        $companies= Company::all();
        $authCompany=Auth::user()->company;
        $role= Auth::user()->is_admin;
        return Inertia::render('Company/Home', compact('users', 'companies', 'authCompany', 'role'));
    }

    public function create()
    {
        if (Auth::user()->company!=null) {
            return Redirect::route('companies.index');
        }
        return Inertia::render('Company/Create');
    }
    public function store(CompanyRequest $request)
    {
        if (Auth::user()->company==null) {
            Auth::user()->company()->create($request->all());
        }
        return Redirect::route('companies.index');
    }

    // public function store(Request $request)
    // {
    //     $request->validate([
    //         'business_name'=>['required'],
    //         'business_email'=>['required', 'email', 'unique:companies,business_email'],
    //         'address'=>['required'],
    //         'cap'=>['required', 'min:5', 'max:5'],
    //         'city'=>['required'],
    //         'province'=>['required'],
    //         'region'=>['required'],
    //     ]);
    //     if (Auth::user()->company()!=null) {
    //         Auth::user()->company()->create($request->all());
    //     }
    //     return Redirect::route('companies.index');
    // }

    public function show(Company $company)
    {
        $auth=Auth::user();
        $show=User::find($company->user_id);
        return Inertia::render('Company/Show', compact('company', 'show', 'auth'));
    }

    public function edit(Company $company)
    {
        if ($company->user_id===Auth::user()->id) {
            return Inertia::render('Company/Edit', compact('company'));
        }else{
            return Redirect::route('companies.index');
        }
    }

    public function update(Request $request, Company $company)
    {
        if ($company->user_id===Auth::user()->id) {
            $company->update($request->all());
        }
        
        return Redirect::route('companies.index');
    }

    public function destroy(Company $company)
    {
        if ($company->user_id===Auth::user()->id) {
            Auth::user()->company()->delete();
        }
        return Redirect::route('companies.index');
    }
}
