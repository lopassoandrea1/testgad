<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public $company;
    public function rules()
    {
        return [
            'business_name'=>'required',
            'business_email'=>'required|email|unique:companies,business_email',
            'address'=>'required',
            'cap'=>'required|digits_between:5,5',
            'city'=>'required',
            'province'=>'required',
            'region'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'business_name.required'=>'Inserire ragione sociale',
            'business_email.required'=>'Inserire una email',
            'business_email.email'=>'Inserire una email valida',
            'business_email.unique'=>'L\'email inserita è gia registrata',
            'address.required'=>'Inserire un indirizzo',
            'cap.required'=>'Inserire un cap valido',
            'cap.digits_between'=>'Il CAP deve contenere 5 numeri',
            'city.required'=>'Inserire città',
            'region.required'=>'Inserire regione',
            'province.required'=>'Inserire provincia',
        ];
    }
}
