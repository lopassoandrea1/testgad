<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class MakeAdmin extends Command
{
    protected $signature = 'command:makeAdmin';
    protected $description = 'Assegna ruolo admin';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $email= $this->ask("Inserisci l'email dell'utente da rendere admin");
        $user= User::where('email', $email)->first();
        if (!$user) {
            $this->error("Utente non trovato");
            return;
        }
        $user->is_admin= true;
        $user->save();
        $this->info("L'utente {$user->name} è ora un admin.");
    }
}
