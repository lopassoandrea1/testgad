<?php

namespace App\Imports;

use App\Models\Company;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CompanyImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Company([
            'user_id'=>Auth::user()->id(),
            'business_name'=> $row['ragione sociale'],
            'business_email'=> $row['email'||'email aziendale'],
            'region'=> $row['regione'],
            'province'=> $row['provincia'],
            'city'=> $row['città'],
            'address'=> $row['indirizzo'],
            'cap'=> $row['cap'],
        ]);
    }
}
