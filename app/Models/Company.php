<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Company extends Model
{
    use HasFactory;

    protected $fillable= [
        'business_name',
        'business_email',
        'address',
        'cap',
        'city',
        'province',
        'region',
        'user_id'
    ];

    public function user(){
        
        return $this->belongsTo(User::class);
    }

}
