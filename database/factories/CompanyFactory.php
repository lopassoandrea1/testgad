<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Company>
 */
class CompanyFactory extends Factory
{
    protected $model = Company::class;

    public function definition()
    {
        $cap = collect([12345, 67890, 25790, 18019, 12342]);
        return [
            'business_name' => $this->faker->name(),
            'business_email' => $this->faker->unique()->safeEmail(),
            'address' => $this->faker->name(),
            'cap'=>$cap->random(),
            'city' => $this->faker->name(),
            'province' => $this->faker->name(),
            'region' => $this->faker->name(),
        ];
    }
}
