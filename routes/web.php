<?php

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\CompanyController;



Route::get('/', [PublicController::class, 'index'])->name('welcome');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

Route::resource('companies', CompanyController::class)
->middleware(['auth:sanctum', 'verified']);

Route::resource('users', UserController::class)
->middleware(['auth:sanctum', 'verified']);
